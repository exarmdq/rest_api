var jwt = require('jsonwebtoken');

module.exports = (req, res, next) => {
    try {
        var token = req.headers.authorization;
        console.log(token);
        var decoded = jwt.verify(token, 'keytoken');
        req.userData = decoded;
        next();
    } catch (error) {
        return res.status(401).json({
            message: 'Auth error'
        });
    }
};