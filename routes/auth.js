var express = require('express');
var router = express.Router();
var Auth = require('../middleware/auth');

var auth_controller = require('../controllers/authController');

router.post('/login', auth_controller.login);
router.post('/logout', Auth, auth_controller.logout);

module.exports = router;