var express = require('express');
var router = express.Router();

var user_controller = require('../controllers/userController');

router.post('/new', user_controller.new);
router.post('/change-password', user_controller.change_password);

module.exports = router;