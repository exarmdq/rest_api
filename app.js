var express = require('express');
var app = express();
var morgan = require('morgan');
var bodyParser = require('body-parser');
var mongoose = require('mongoose');

var userRoutes = require('./routes/users');
var authRoutes = require('./routes/auth');

mongoose.connect('mongodb+srv://test:test123@rest-api-fjtbf.mongodb.net/test?retryWrites=true',{ useNewUrlParser: true });

app.use(function(req, res, next) {
	res.header('Access-Control-Allow-Origin', '*');
  	res.header('Access-Control-Allow-Headers', 'Origin, X-Requested-With, Content-Type, Accept, Authorization');  

	if(req.method === 'OPTIONS'){
		res.header('Access-Control-Allow-Methods', 'PUT, POST, DELETE, GET')
		return res.status(200);
	}
	next();
});

app.use(morgan('dev'));
app.use(bodyParser.urlencoded({extended: false}));
app.use(bodyParser.json());

app.use('/auth', authRoutes);
app.use('/user', userRoutes);


app.use(function(req, res, next) {
    var error = new Error('Not found');
    error.status = 404;
    next(error);
});

app.use(function(err, req, res, next) {
    res.status(err.status || 500);
    res.json({
        err: {
            message: err.message
        }
    });
});
  
module.exports = app;