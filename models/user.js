var mongoose = require('mongoose');

var userSchema = mongoose.Schema({
    _id: mongoose.Schema.Types.ObjectId,
    id:{type: Number},
    type:{type: Number, require: true},
    responsability:{type: Number},
    fullname:{type: String, require: true},
    phone:{type: String, require: true},
    email: {type: String, require: true, unique: true},
    password: {type: String, require: true}
})

module.exports = mongoose.model('User', userSchema);