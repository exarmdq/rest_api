var mongoose = require('mongoose');
var User = require('../models/user');
var bcrypt = require('bcrypt');
var jwt = require('jsonwebtoken');

exports.login = function(req, res){
    var data = req.body;

    User.findOne({email: data.email})
    .then(function(user){
        if(user.length<1){
            return res.status(401).json({
                message: 'fallo login'
            });
        }
        bcrypt.compare(data.password, user.password, function(err, response){
            if(err){
                return res.status(401).json({
                    message: 'fallo login'
                });
            }

            if(response){
                var access_token = jwt.sign({}, 'keytoken',{expiresIn: '1h'});
                console.log(access_token);
                res.status(200).json({
                    data: {
                        user:{
                            id: user.id,
                            type: user.type,
                            responsability: user.responsability
                        },
                        access_token: access_token
                    },
                    user:{
                        fullname: user.fullname,
                        email: user.email,
                        phone: user.phone,
                        type: user.type,
                        id: user.id,
                        responsability: user.responsability
                    },
                    access_token: access_token
                });
            }else{
                return res.status(401).json({
                    message: 'fallo login'
                });
            }           
            
        });
    })
    .catch(function(err){
        console.log(err);
        res.status(500).json({
            error: err
        });    
    });   
}

exports.logout = function(req, res){
    return res.status(200).json({
        message: 'logout'
    });
}